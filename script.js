const cars = require("./cars.json");

const cars2010 = cars.filter((car) => car.year > 2010);
console.log(cars2010);

const carsRed = cars.filter((car) => car.color == "Red");
console.log(carsRed);

const carsToString = cars2010.map(
  (item) => item.brand + " - " + item.model + " " + item.year
);
console.log(carsToString);

const newCars = cars.map((item) => {
  let { id, brand, model, year, color } = item;
  let current = {};
  if (item.brand == "Jaguar" || item.brand == "Aston Martin") {
    current = { id, brand, model, year, color, luxuryTax: true };
  } else {
    current = { id, brand, model, year, color, luxuryTax: false };
  }
  return current;
});

const carsTest = newCars.filter((car) => car.luxuryTax == true);
console.log(carsTest);

const findCars = (year) =>
  cars
    .filter((item) => item.year == year)
    .map((item) => item.brand + " - " + item.year);
console.log(findCars(2012));